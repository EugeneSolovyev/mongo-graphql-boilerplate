const AppDispatcher = require('~/client/dispatcher/AppDispatcher');
const NoticeConstants = require('~/client/constants/NoticeConstants');

module.exports = {
  create: ({ notice, viewer }) => {
    AppDispatcher.dispatch({
      type: NoticeConstants.ADD_NOTICE,
      notice,
      viewer,
    });
  },
};
