import React from 'react';
import _ from 'lodash';

export default class Application extends React.Component {
  constructor(props) {
    super(props);
    this.renderChildren = this.renderChildren.bind(this);
  }
  
  renderChildren() {
    const { location } = this.props;
    const isLoginPage = _.includes(location.pathname, '/auth');
    const hasViewer = _.get(this.props, 'viewer.id');
    
    if (hasViewer || isLoginPage) {
      return this.props.children;
    } else {
      this.props.router.push('/auth');
    }
  }
  
  render() {
    return (
      <main>
				{ this.renderChildren() }
			</main>
    );
  }
}