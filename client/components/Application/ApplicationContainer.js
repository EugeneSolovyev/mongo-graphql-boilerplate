import Relay from 'react-relay/classic';
import Application from './ApplicationComponent';

export default Relay.createContainer(Application, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on User {
          id
          username
      }
    `
  }
});