import React from 'react';
import { httpClient } from '~/client/http-client';

export default class Authenticate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
    this.updateStateValue = this.updateStateValue.bind(this);
    this.signIn = this.signIn.bind(this);
  }
  
  updateStateValue({ target }) {
    this.setState({
      [target.name]: target.value,
    });
  }
  
  signIn() {
    httpClient.post('/signin', this.state)
      .then(({ data }) => {
        if (data) window.location.href = process.env.API_DOMAIN;
      })
      .catch(error => console.log(error));
  }
  
  render() {
    return (
      <div>
        <h5>Sign In/Sign Up</h5>
        <div>
          <label htmlFor="username">Username:</label>
          <input
            type="text"
            id='username'
            name='username'
            value={this.state.username}
            onChange={this.updateStateValue}
          />
        </div>
        <div>
          <label htmlFor="password">Password:</label>
          <input
            type="text"
            id='password'
            name='password'
            value={this.state.password}
            onChange={this.updateStateValue}
          />
        </div>
        <div>
          <button onClick={this.signIn}>Sign In</button>
        </div>
      </div>
    );
  }
}