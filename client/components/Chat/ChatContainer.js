import Relay from 'react-relay/classic';
import Chat from './ChatComponent';

export default Relay.createContainer(Chat, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on User {
        id
        username
      }
    `
  }
});
