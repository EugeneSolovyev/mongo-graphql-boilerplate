import React from 'react';
import NoticeStore from '~/client/stores/NoticeStore';
const NoticeActions = require('~/client/actions/NoticeActions');
import { Link } from 'react-router';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notice: '',
    };
    this.updateNotice = this.updateNotice.bind(this);
    this.commitUpdate = this.commitUpdate.bind(this);
  }

  updateNotice({ target }) {
    this.setState({
      [target.name]: target.value,
    });
  }

  commitUpdate() {
    NoticeActions.create({ viewer: this.props.viewer, notice: this.state.notice });
    this.setState({ notice: '' });
  }

  render() {
    const { viewer } = this.props;
    const { notice } = this.state;
    return (
      <div>
        <h5>Hello, {viewer.username}</h5>
        <Link to='chat'>Chat</Link>
        <div className="formToAddNotice">
          <div className="formToAddNotice__textField">
            <input
              type="text"
              name='notice'
              value={notice}
              onChange={this.updateNotice}
            />
          </div>
          <div className="formToAddNotice__btn">
            <button onClick={this.commitUpdate}>Add</button>
          </div>
        </div>
        <div className="notices">
          {viewer.notices.edges.map(({ node }) => (
            <div key={node.id}>{node.notice}</div>
          ))}
        </div>
      </div>
    );
  }
}