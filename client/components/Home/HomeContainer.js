import Relay from 'react-relay/classic';
import AddUserNoticeMutation from '~/client/mutations/addUserNoticeMutation';
import Home from './HomeComponent';

export default Relay.createContainer(Home, {
  initialVariables: {

  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on User {
        id,
        username,
        notices(first: 100) {
          edges {
            node {
              id,
              notice,
              created_at
            }
          }
        },
        ${AddUserNoticeMutation.getFragment('viewer')},
      }
    `
  }
});
