import React from 'react';

export default class Loading extends React.Component {
  render() {
    return (
      <div className='loader-box'>
        <div className='loader'>Loading...</div>
      </div>
    );
  }
}