const keyMirror = require('keymirror');

module.exports = keyMirror({
  ADD_NOTICE: null,
});