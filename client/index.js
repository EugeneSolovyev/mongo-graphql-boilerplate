import React from 'react';
import Relay from 'react-relay/classic';
import ReactDOM from 'react-dom';
import { browserHistory, applyRouterMiddleware, Router } from 'react-router';
import useRelay from 'react-router-relay';
import Route from './routes/Route';
import './styles/app.sass';

const rootNode = document.getElementById('root');

Relay.injectNetworkLayer(
  new Relay.DefaultNetworkLayer(`${process.env.API_DOMAIN}/graphql`, {
    credentials: 'same-origin',
    retryDelays: [50000],
  })
);

ReactDOM.render(
  <Router
    history={browserHistory}
    routes={Route}
    render={applyRouterMiddleware(useRelay)}
    environment={Relay.Store}
  />,
  rootNode
);