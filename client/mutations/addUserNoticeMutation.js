import Relay from 'react-relay/classic';

export default class AddUserNoticeMutation extends Relay.Mutation {
  static fragments = {
    viewer: () => Relay.QL`
      fragment on User {
        id,
      }
    `,
  };

  getMutation() {
    return Relay.QL`mutation { addUserNoticeMutation }`;
  }

  getVariables() {
    return {
      notice: this.props.notice,
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on AddUserNoticePayload @relay(pattern: true) {
        viewer {
          id
        }
        newNotice {
          node {
            id
            notice
            created_at
          }
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'RANGE_ADD',
        parentName: 'viewer',
        parentID: this.props.viewer.id,
        connectionName: 'notices',
        edgeName: 'newNotice',
        rangeBehaviors: args => 'append',
      },
    ];
  }

  getOptimisticResponse() {
    return {
      viewer: {
        id: this.props.viewer.id,
      },
    };
  }
}