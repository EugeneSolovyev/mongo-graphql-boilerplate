import Relay from 'react-relay/classic';

export const ViewerQuery = {
	viewer: () => Relay.QL`
    query { viewer }
  `,
};

export const NodeQuery = {
	viewer: Component => Relay.QL`
    query {
      viewer {
          ${Component.getFragment('viewer')}
      }
    }
  `,
};