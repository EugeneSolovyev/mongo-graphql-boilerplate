import React from 'react';
import { Route, IndexRoute, Redirect, browserHistory } from 'react-router';

import LoadingComponent from '~/client/components/Loading/LoadingComponent';
import AuthenticationComponent from '~/client/components/Authenticate/AuthenticateComponent';
import ApplicationContainer from '~/client/components/Application/ApplicationContainer';
import HomeContainer from '~/client/components/Home/HomeContainer';
import ChatContainer from '~/client/components/Chat/ChatContainer';

import { ViewerQuery } from './Queries';

export default (
  <Route
    history={browserHistory}
    path="/"
    component={ApplicationContainer}
    queries={ViewerQuery}
    render={({ props }) =>
      props ? <ApplicationContainer {...props} /> : <LoadingComponent/>
    }>
    <IndexRoute
      component={HomeContainer}
      queries={ViewerQuery}
      render={({ props }) =>
        props ? <HomeContainer {...props} /> : <LoadingComponent/>
      }
    />

    <Route
      path='chat'
      component={ChatContainer}
      render={({props}) => props ? <ChatContainer {...props} /> : <LoadingComponent />}
    />

    <Route
      path='auth'
      component={AuthenticationComponent}
      render={({ props }) =>
        props ? <AuthenticationComponent {...props} /> : <LoadingComponent/>
      }
    />

    <Redirect from="*" to="/"/>
  </Route>
);