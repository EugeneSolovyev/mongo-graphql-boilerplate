import Relay from 'react-relay/classic';

const assign = require('object-assign');
const EventEmitter = require('events').EventEmitter;
import AddUserNoticeMutation from '~/client/mutations/addUserNoticeMutation';
const NoticeConstants = require('~/client/constants/NoticeConstants');
const AppDispatcher = require('../dispatcher/AppDispatcher');

const CHANGE_EVENT = 'change';

const NoticeStore = assign({}, EventEmitter.prototype, {
  emitChange() {
    this.emit(CHANGE_EVENT);
  },
  addChangeListener(callback) {
    this.on(CHANGE_EVENT, callback);
  },
  removeChangeListener(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },
});

const onSuccess = (action) => {
  console.log('Success');
};

const onFailure = (transaction, action) => {
  console.error('Error');
};

AppDispatcher.register((action) => {
  switch (action.type) {
    case NoticeConstants.ADD_NOTICE:
      Relay.Store.commitUpdate(
        new AddUserNoticeMutation({
          viewer: action.viewer,
          notice: action.notice,
        }),
        { onSuccess: () => onSuccess(action), onFailure: transaction => onFailure(transaction, action) },
      );
      NoticeStore.emitChange();
      break;
    default:
    //
  }
});

module.exports = NoticeStore;
