import _ from 'lodash';
import logger from '~/server/logger';
import httpClient from '~/server/http-client';

const handleError = (error, callback) => {
  logger.error(error);
  return callback(error);
};

const loginUser = (login, password, callback) => {
  httpClient.post('/auth/sign_in', { login, password })
    .then(({ result }) => callback(null, result))
    .catch(error => handleError(error, callback));
};

const registerUser = (username, email, password, callback) => {
  httpClient.post('/auth/sign_up', { username, email, password })
    .then(user => callback(null, user))
    .catch(error => handleError(error, callback));
};
const logoutUser = (api_key, callback) => {
  httpClient.post('/auth/logout', { api_key })
    .then(() => callback(null))
    .catch(error => handleError(error, callback));
};
const handleSocialAuth = (accessToken, refreshToken, profile, callback) => {
  const { emails, displayName, provider } = profile;
  httpClient.post('/auth/social', {
    provider,
    email: _.get(emails, '[0].value'),
    username: displayName.replace(/\s/g, ''),
  })
    .then(({ result }) => callback(null, result))
    .catch(error => handleError(error, callback));
};

module.exports = {
  loginUser,
  registerUser,
  logoutUser,
  handleSocialAuth,
};