const session = require('express-session');
const bodyParser = require('body-parser');
const passport = require('passport');
const mongoose = require('mongoose');
const Authentication = require('./passport');
const config = require('~/server/config/environment/index.js').default;
const MongoStore = require('connect-mongo')(session);

mongoose.connect('mongodb://localhost:27017/GRAPH_MONGOOSE', { useNewUrlParser: true });
const installedMongoDB = mongoose.connection;

const installedSession = session({
  secret: config.SESSION_SECRET,
  store: new MongoStore({ mongooseConnection: installedMongoDB }),
  cookie: {
    maxAge: config.SESSION_MAX_AGE,
  },
  resave: false,
  saveUninitialized: false,
});
const installedBodyParserUrlEncoded = bodyParser.urlencoded({ extended: false });
const installedBodyParserJson = bodyParser.json();
const installedPassport = passport.initialize();
const installedPassportSession = passport.session();

module.exports = (expressApp, callOnly) => {
  expressApp.use(installedBodyParserUrlEncoded);
  expressApp.use(installedBodyParserJson);
  expressApp.use(installedSession);
  expressApp.use(installedPassport);
  expressApp.use(installedPassportSession);

  installedMongoDB
    .on('error', console.error.bind(console, 'connection error:'))
    .once('open', () => console.log('connection with db established'));

  if (callOnly) return expressApp;

  Authentication(expressApp);
  return expressApp;
};
