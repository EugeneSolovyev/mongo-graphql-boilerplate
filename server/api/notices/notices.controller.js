const NoticesModel = require('./notices.model');

module.exports = {
  create: async (notice, user_id) => {
    const NewNotice = new NoticesModel({ user_id, notice });
    return await NewNotice.save();
  },
  getAll: async () => await NoticesModel.find({}),
  get_all: (user_id, callback) => {
    NoticesModel.find({ user_id })
      .populate('user_id')
      .exec((err, doc) => {
        callback(err, doc);
      });
  },
};
