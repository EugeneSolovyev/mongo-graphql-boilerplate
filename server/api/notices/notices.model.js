const mongoose = require('mongoose');
const moment = require('moment');
const Schema = mongoose.Schema;

const schema = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  notice: {
    type: Schema.Types.String,
    required: true,
  },
  created_at: {
    type: Schema.Types.String,
    default: moment().format('DD MMMM YYYY HH:mm:ss'),
  },
});

const NoticeModel = mongoose.model('Notice', schema);
module.exports = NoticeModel;