const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const UserModel = require('../models');

module.exports = {
  create: (req, res, next) => {
    const { username, password } = req.body;
    new UserModel({ username, password })
      .save((err, doc) => {
        if (err || !doc) return next(err);

        res.json({ status: 'success', message: 'User added successfully!!!', data: null });
      });
  },
  authenticate: (req, res, next) => {
    const { username, password } = req.body;
    UserModel.findOne({ username }, (err, doc) => {
      if (err) return next(err);
      if (!bcrypt.compareSync(password, doc.password)) {
        return res.json({
          status: 'error',
          message: 'Invalid username or password',
          data: null,
        });
      }

      const token = jwt.sign({ id: doc._id }, 'test_secret');
      res.json({ status: 'success', message: 'OK', data: { user: doc, token } });
    });
  },
};
