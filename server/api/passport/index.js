const passport = require('passport');
const AuthController = require('./controllers/auth.controller');
const { installedLocalStrategy } = require('~/server/api/passport/strategies');

passport.use(installedLocalStrategy);

passport.serializeUser((user, callback) => {
  callback(null, user);
});

passport.deserializeUser((user, callback) => {
  callback(null, user);
});

module.exports = (app) => {
  app.post('/signin', passport.authenticate('local'), AuthController.authenticate);
  app.post('/signup', AuthController.create);
};