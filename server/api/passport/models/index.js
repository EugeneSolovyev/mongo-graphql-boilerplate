const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const Schema = mongoose.Schema;
const saltRound = 10;

const schema = new Schema({
  username: {
    type: Schema.Types.String,
    unique: true,
    required: true,
  },
  password: {
    type: Schema.Types.String,
    required: true,
  },
  notices: [{
    type: Schema.Types.ObjectId,
    ref: 'Notice'
  }],
});

schema.pre('save', function (next) {
  this.password = bcrypt.hashSync(this.password, saltRound);
  next();
});

const UserModel = mongoose.model('User', schema);
module.exports = UserModel;
