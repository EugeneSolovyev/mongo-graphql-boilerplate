const LocalStrategy = require('passport-local').Strategy;
const UserModel = require('~/server/api/passport/models');

const installedLocalStrategy = new LocalStrategy((username, password, done) => {
  UserModel.findOne({ username })
    .then((user) => {
      if (user) return done(null, user);

      new UserModel({ username, password })
        .save((err, user) => {
          if (err) return done(err);

          done(null, user);
        });
    });
});

module.exports = installedLocalStrategy;
