const _ = require('lodash');

class User {
  constructor(params) {
    _.assign(this, params);
  }
}

module.exports = {
  User,
};