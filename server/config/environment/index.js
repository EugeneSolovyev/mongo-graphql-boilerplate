import _ from 'lodash';

const config = {
  env: process.env.NODE_ENV || 'development',
  port: process.env.PORT || 3000,
  SESSION_SECRET: process.env.SESSION_SECRET || '123',
  SESSION_MAX_AGE: process.env.SESSION_MAX_AGE || 2592000000, // one month
  graphql: {
    port: process.env.GRAPHQL_PORT || 8888
  }
};

export default _.extend(config, require(`./${config.env}`).default);