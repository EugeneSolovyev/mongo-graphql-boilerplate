
module.exports = {
  rewrites: [
    { from: /^\/auth\/google$/, to: '/auth/google' },
    { from: /\/auth\/google\/redirect*/, to: '/auth/google/redirect' },
    { from: /^\/auth\/facebook$/, to: '/auth/facebook' },
    { from: /\/auth\/facebook\/callback*/, to: '/auth/facebook/callback' },
    { from: /^\/auth\/twitter$/, to: '/auth/twitter' },
    { from: /\/auth\/twitter\/redirect*/, to: '/auth/twitter/redirect' },
  ]
};