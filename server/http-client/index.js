import axios from 'axios';
import _ from 'lodash';

require('dotenv').config();

const getUrlWithParams = (url, params) => {
	const chunks = [];
	for (const key in params) {
		const chunk = `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`;
		chunks.push(chunk);
	}
	if (chunks.length) return `${url}?${chunks.join('&')}`;
	return url;
};

const instance = axios.create({
	baseURL: process.env.REMOTE_API_DOMAIN,
	headers: {
		'Content-Type': 'application/json',
	},
});

instance.interceptors.response.use(response => response.data, error => Promise.reject(error));

const httpClient = {
	get: (url, params) => instance.get(getUrlWithParams(url, params))
		.then(response => response)
		.catch(error => Promise.reject(error || 'Error')),
	post: (url, payload) => instance.post(url, payload)
		.then(response => response)
		.catch(error => Promise.reject(error || 'Error')),
	put: (url, payload) => instance.put(url, payload)
		.then(response => response)
		.catch(error => Promise.reject(error || 'Error')),
	delete: (url, payload) => instance.delete(url, payload)
		.then(response => response)
		.catch(error => Promise.reject(error || 'Error')),
};

export default httpClient;