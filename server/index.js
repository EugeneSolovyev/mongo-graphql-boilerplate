const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const chalk = require('chalk');
const express = require('express');
const path = require('path');
const webpackConfig = require('../webpack.dev');
const setupAuthentication = require('./api');
const historyApiFallback = require('connect-history-api-fallback');
const graphQLHTTP = require('express-graphql');
import schema from './schema/schema';
import config from './config/environment';
const RouteExceptions = require('~/server/helpers/route-exceptions');

if (config.env === 'development') {
  const graphql = express();
  setupAuthentication(graphql, true);
  graphql.use('/graphql', graphQLHTTP(req => ({
    graphiql: true,
    pretty: true,
    schema,
    context: req,
  })));
  graphql.listen(config.graphql.port, () => console.log(chalk.green(`GraphQL is listening on port ${config.graphql.port}`)));

  const relayServer = new WebpackDevServer(webpack(webpackConfig), {
    contentBase: '/build/',
    proxy: {
      '/graphql': `http://localhost:8888/graphql`
    },
    stats: {
      colors: true
    },
    hot: true,
    historyApiFallback: RouteExceptions,
  });
  relayServer.use('/', setupAuthentication(express(), false));
  relayServer.use('/', express.static(path.join(__dirname, '../build')));
  relayServer.listen(config.port, () => console.log(chalk.green(`Relay is listening on port ${config.port}`)));
} else {
  const relayServer = express();
  relayServer.use(historyApiFallback(RouteExceptions));
  relayServer.use('/', express.static(path.join(__dirname, '../build/app')));
  setupAuthentication(relayServer, false);
  relayServer.use('/graphql', graphQLHTTP(req => ({
    schema,
    context: req
  })));
  relayServer.listen(config.port, () => console.log(chalk.green(`Relay is listening on port ${config.port}`)));
}