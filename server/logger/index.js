const { createLogger, format, transports } = require('winston');
require('winston-daily-rotate-file');
const fs = require('fs');

const env = process.env.NODE_ENV || 'development';

const logDir = './server/log';

if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}

const logMessage = info => `${info.timestamp} ${info.level}: ${info.message}`;
const dailyRotateTransport = new transports.DailyRotateFile({
  filename: `${logDir}/%DATE%-logs.log`,
  datePattern: 'YYYY-MM-DD',
});

const logger = createLogger({
  level: env === 'development' ? 'debug' : 'info',
  format: format.combine(
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss',
    }),
    format.printf(logMessage),
  ),
  transports: [
    new transports.Console({
      level: 'info',
      format: format.combine(
        format.colorize(),
        format.printf(logMessage),
      ),
    }),
    dailyRotateTransport,
  ],
  exitOnError: false,
});

module.exports = logger;