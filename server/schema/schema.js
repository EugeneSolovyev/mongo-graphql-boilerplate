const _ = require('lodash');
const { User } = require('~/server/api/user');
const NoticeController = require('~/server/api/notices/notices.controller');

const {
  GraphQLString,
  GraphQLInt,
  GraphQLList,
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLSchema,
  GraphQLID,
} = require('graphql');

const {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  connectionFromPromisedArray,
  fromGlobalId,
  globalIdField,
  mutationWithClientMutationId,
  nodeDefinitions,
  cursorForObjectInConnection,
} = require('graphql-relay');

const { nodeInterface, nodeField } = nodeDefinitions((globalID, req) => {
  const { type, id } = fromGlobalId(globalID);
  if (type === 'User') {
    return new User(req.user);
  }
  return null;
}, (obj) => {
  if (obj instanceof User) {
    return UserType;
  }
  return null;
});

const UserType = new GraphQLObjectType({
  name: 'User',
  description: 'User',
  fields: () => ({
    id: globalIdField('User'),
    username: { type: GraphQLString },
    notices: {
      type: NoticeConnection,
      args: connectionArgs,
      resolve: (obj, args) => connectionFromPromisedArray(NoticeController.getAll(), args),
    },
  }),
  interfaces: [nodeInterface],
});

const NoticeType = new GraphQLObjectType({
  name: 'Notices',
  description: 'User\'s notices',
  fields: () => ({
    id: globalIdField('Notice'),
    notice: { type: GraphQLString },
    created_at: { type: GraphQLString },
  }),
});

const { connectionType: NoticeConnection, edgeType: noticeEdge } = connectionDefinitions({
  name: 'Notices',
  nodeType: NoticeType,
});

const addUserNoticeMutation = mutationWithClientMutationId({
  name: 'AddUserNotice',
  inputFields: {
    notice: { type: GraphQLString },
  },
  mutateAndGetPayload: async ({ notice }, { user }) => {
    const node = await NoticeController.create(notice, user._id);
    const all = await NoticeController.getAll();
    return {
      node,
      all: all.map(item => item._id.toString()),
    };
  },
  outputFields: {
    viewer: {
      type: UserType,
      resolve: (payload, args, req) => req.user,
    },
    newNotice: {
      type: noticeEdge,
      resolve: ({ node, all }) => ({
        node,
        cursor: cursorForObjectInConnection(all, node._id.toString())
      }),
    },
  },
});

const queryType = new GraphQLObjectType({
  name: 'Query',
  fields: () => ({
    node: nodeField,
    viewer: {
      type: UserType,
      resolve: (obj, args, req) => req.user,
    },
  }),
});

const mutationType = new GraphQLObjectType({
  name: 'mutation',
  fields: () => ({
    addUserNoticeMutation,
  }),
});

export default new GraphQLSchema({
  query: queryType,
  mutation: mutationType,
});
