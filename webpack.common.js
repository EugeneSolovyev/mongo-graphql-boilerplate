const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Dotenv = require('dotenv-webpack');

module.exports = {
	module: {
    rules: [{
      test: /\.jsx?$/,
      use: 'babel-loader',
      exclude: /node_modules/
    }, {
      test: /\.css$/,
      use: [
        'style-loader',
        'css-loader'
      ],
    }, {
      test: /\.(scss|sass)$/,
      use: [
        'style-loader',
        {
          loader: 'css-loader',
          options: {
            importLoaders: 1,
            localIdentName: '[name]__[local]___[hash:base64:5]',
          }
        },
        {
          loader: 'postcss-loader',
          options: {
            // https://github.com/postcss/postcss-loader/issues/164
            // use ident if passing a function
            ident: 'postcss',
            plugins: () => [
              /* eslint-disable global-require */
              require('precss'),
              require('autoprefixer')
            ]
          }
        },
        'sass-loader',
      ]
    }, {
      test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)$/,
      use: [
        {
          loader: 'url-loader',
          options: {
            limit: 1000,
            name: 'assets/[hash].[ext]'
          }
        }
      ]
    }, {
      test: /\.(woff|woff2|eot|ttf)$/,
      use: [
        { loader: 'file-loader' }
      ]
    }]
  },
  plugins: [
    new CleanWebpackPlugin(['build']),
    new HtmlWebpackPlugin({
      title: 'I need title ...',
      template: path.resolve(__dirname, 'client', 'index.html'),
      mobile: true,
      inject: false
    }),
    new Dotenv(),
  ],
  output: {
    path: path.join(__dirname, 'build', 'app'),
    publicPath: '/',
    filename: '[name].js'
  }
};