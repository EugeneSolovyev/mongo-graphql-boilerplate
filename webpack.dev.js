const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
	mode: 'development',
	entry: {
		app: [
			'react-hot-loader/patch',
			path.join(__dirname, 'client/index.js'),
			'webpack-dev-server/client?http://localhost:3000',
			'webpack/hot/only-dev-server'
		],
		vendor: ['react', 'react-dom']
	},
	devtool: 'source-map',
	plugins: [
		new webpack.NoEmitOnErrorsPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin(),
		new webpack.DefinePlugin({
			__DEV__: true
		})
	],
});