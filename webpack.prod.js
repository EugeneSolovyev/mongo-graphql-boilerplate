const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = merge(common, {
	mode: 'production',
	entry: {
		app: [
			path.join(__dirname, 'client/index.js')
		],
		vendor: ['react', 'react-dom']
	},
	devtool: 'source-map',
	plugins: [
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify('production')
			}
		})
	],
	optimization: {
		minimizer: [
			new UglifyJsPlugin({
				cache: false,
				uglifyOptions: {
					ie8: false,
				},
			})
		]
	}
});